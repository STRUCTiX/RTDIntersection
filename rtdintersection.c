#include <stdio.h>
#include "rtdintersection.h"
#include <stdlib.h>
#include "prng.h"
#include <math.h>

#include <signal.h>
#include <time.h>

#define VARIATION 0.1
#define CYCLES 9000000

student *studentlist;

void sig_handler(int signo) {
    if (signo == SIGINT) {
        exit(0); //This will call the atexit function
    }
}

void exitfunc(void) {
    free(studentlist);
    printf("Done.\n");
}

int main(int argc, char *argv[]) {
    exam exprop;
        
    /* Initialize the random number generator */
    InitializePRNG(time(NULL));
    
    atexit(exitfunc);
    
    if (signal(SIGINT, sig_handler) == SIG_ERR) {
         printf("\ncan't catch SIGINT\n");
    }
    
    /**
     * Do all user input an student memory allocation
     * Should be checked for errors
     */
    
    inputExamProperties(&exprop);

    studentlist = malloc(exprop.students * sizeof(student));
        
    generatePoints(studentlist, &exprop);
    
    outputResult(&exprop, studentlist);
        
    /**
     * To free the allocated memory,
     * set a global pointer to the array and free it
     * in the exitfunc()
     */
    
    return 0;
}

int inputExamProperties(exam *ex) {
    int i, maxpts = 0;
    printf("Number of tasks: ");
    scanf("%i", &ex->tasks);
    printf("Expected total average: ");
    scanf("%f", &ex->average);

    
    //ex->maxpoints = malloc(ex->tasks * sizeof(int));
        
    for (i = 0; i < ex->tasks; i++) {
        printf("Max. points of task %i: ", (i + 1));
        scanf("%i", &ex->maxpoints[i]);
        maxpts += ex->maxpoints[i];
    }
    //Cumulated points
    ex->maxpointstotal = maxpts;
    
    printf("Number of students: ");
    scanf("%i", &ex->students);
    
    return 1; //Should return an error code
}


int generatePoints(student *list, exam *ex) {
    float calculatedAverage = 0.0, minAverage, maxAverage;
    int i, j, k, pts;
    long stack, counter = 0;
    
    /* First 9mio cycles we're trying to get the exact desired average */
    minAverage = maxAverage = ex->average;
        
    while (calculatedAverage < minAverage || calculatedAverage > maxAverage) {
        /* Initialize all tasks with random points */
        for (i = 0; i < ex->students; i++) {
            pts = 0;
            for (j = 0; j < ex->tasks; j++) {
                list[i].points[j] = getrandom(0, ex->maxpoints[j]);
                pts += list[i].points[j];
            }
            /* calculate the grade of a student */
            list[i].grade = calculateGrade(pts, ex->maxpointstotal);
        }
        stack = 0;
        for (k = 0; k < ex->students; k++) {
            stack += list[k].grade;
        }
        calculatedAverage = (float) stack / (float) ex->students;
        if (counter++ >= CYCLES) {
            printf("This might take longer than expected.\nThis message will appear every 9mio iterations.\nYou might have to increase the max. points per task.\n");
            counter = 0;
            
            /* Increase the variation of the accepted average */
            minAverage -= VARIATION;
            if (minAverage < 1.0) {
                minAverage = 1.0;
            }
            maxAverage += VARIATION;
            if (maxAverage > 6.0) {
                maxAverage = 6.0;
            }
            printf("Variation increased. Accepted average: %.2f - %.2f\n", minAverage, maxAverage);
        }
    }
    return 1;
}

void outputResult(exam *ex, student *list) {
    int i, z, total = 0;
    for (i = 0; i < ex->students; i++) {
        printf("Student %02i: ", (i + 1));
        for (z = 0; z < ex->tasks; z++) {
            printf("%02i ", list[i].points[z]);
        }
        printf("Grade: %.2f\n", list[i].grade);
        total += list[i].grade;
    }
    printf("Total average: %.2f\n", (total / (float) ex->students));
}

float calculateGrade(int points, int maxpoints) {
    return fabs((((float)points / (float)maxpoints) * 5.0) - 6.0);
}
