/**
 * Pseudo random number generator using the Mersenne Twister algorithm
 * This code was taken from wikipedia: https://en.wikipedia.org/wiki/Mersenne_Twister
 */
#include <stdint.h>

void InitializePRNG(const uint32_t  seed);
uint32_t ExtractU32();
int getrandom(int low, int high);
