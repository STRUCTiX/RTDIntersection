


typedef struct {
    float grade;
    int points[10];
} student;

typedef struct {
    int students;
    float average;
    int tasks;
    int maxpointstotal;
    int maxpoints[10];
} exam;


int inputExamProperties(exam *ex);
int generatePoints(student *list, exam *ex);
float calculateGrade(int points, int maxpoints);
void outputResult(exam *ex, student *list);
