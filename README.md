# RTDIntersection
## General information
This program will randomly generate a bunch of grades. This is just a fun project and is not intended for any productive use.

The main purpose of creating this was to see how plausible it would be using such a software as a teacher. 

It will be updated or not depending on my creativity and interest.

## Documentation
[Pseudo random number generator algorithm](https://en.wikipedia.org/wiki/Mersenne_Twister)
