#Infos: http://www.ijon.de/comp/tutorials/makefile.html

VERSION = 1.0
CC      = /usr/bin/gcc
CFLAGS  = -Wall -g -D_REENTRANT -DVERSION=\"$(VERSION)\" 
#LDFLAGS = -lm -lpthread `gtk-config --cflags` `gtk-config --libs` -lgthread
LDFLAGS = -lm

OBJ = rtdintersection.o prng.o

lcdclock: $(OBJ)
	$(CC) $(CFLAGS) -o rtdintersection $(OBJ) $(LDFLAGS)
        
%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -r *.o

install:
	sudo cp -r lcdclock /usr/local/bin
